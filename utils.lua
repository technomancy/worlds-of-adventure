return {
  member = function(x, t)
    for _,y in ipairs(t) do
      if x == y then return true end
    end
  end,

  split = function(input, separator)
    separator = separator or " "
    local t={}
    for str in string.gmatch(input, "([^"..separator.."]+)") do
      table.insert(t, str)
    end
    return t
  end
}