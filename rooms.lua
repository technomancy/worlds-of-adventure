local rooms = {}

-- name
-- description
-- exits
-- items?
-- actions

rooms["Your bedroom"] = {
  name = "Your bedroom",
  description = "You see a bed and you also see a picture of you.",
  exits = {down = "Living room"},
  actions = {
    ["take picture"] = "It is stuck to the wall.",
    ["lay down in bed"] = "You go to sleep, then you wake up refreshed.",
  }
}

rooms["Living room"] = {
  name = "Living room",
  description = "There is a couch and a table. An oven is in the corner. There is a starway up and a door to the east.",
  exits = {
    up = "Your bedroom",
    east = "Outside cottage",
  },
}
rooms["Outside cottage"] = {
  name = "Outside cottage",
  description = "You see your cottage. There is a fence with a gate in it. You see a lot of trees around. In the distance to the north, the road has been blocked by a landslide, but you can follow it a short distance. To the south is a forest.",
  exits = {
    west = "Living room",
    north = "Landslide",
    up = "Landslide",
    south = "Forest"
  },
}
rooms["Landslide"] = {
  name = "Landslide",
  description = "You are standing next to a landslide covering a broad country road, an unstable pile of rock and dirt. To the south, the road leads back to the cottage. To the east looms a huge ancient stone castle. You see a tunnel",
  exits = {
    south = "Outside cottage",
    down = "Outside cottage",
    east = "Castle door",
    north = "tunnel",}
}
rooms["tunnel"] = {
  name = "tunnel",
  description = "this is a cool underground tunnel with high walls of gold and iron and stone and silver and diamond. You can't believe How high of looming gold and iron and stone and silver and diamond.",
  exits = {
    south = "Landslide"
  },
  items = { ["goldbar"] = true, },
}

rooms["Castle door"] = {
  name = "Castle door",
  description = "You see a locked door in front of you. To the north is the foot of a looming cliff. Away to the southwest you can see a road that has been blocked by a landslide. ",
  exits = {
    north = "Foot of cliff",
    southwest = "Landslide",
  },
}

rooms["Foot of cliff"] = {
  name = "Foot of cliff",
  description = "The cliff looms up above you. South is the castle door.",
  exits = {
    south = "Castle door",
    up = "Top of cliff"
  },
}

rooms["Top of cliff"] = {
  name ="Top of cliff",
  description = "You are at the top of the cliff. You see a window to the south.",
  exits = {
    down = "Foot of cliff",
  },
}

rooms["Forest"] = {
  description = "This is a forest full of trees. There is a huge rock next to you.",
  exits = {
    north = "Outside cottage"
  },
  actions = {
    ["push rock"] = function(player, rooms)
      print("A passage opens under the rock.")
      local r = rooms["Forest"]
      r.exits["down"] = "Hidden room"
      r.description = "This is a forest full of trees. The rock has been moved, uncovering a hole."
    end,
    ["lay down on rock"] = "It's too hard to lay on!",
  }
}



rooms["Hidden room"] = {
   description = "",
   exits = {
     up = "Forest"
   }
   
   
   
   
   
   
   
   
   
   
   
}










for name, room in pairs(rooms) do
  if(room.actions == nil) then room.actions = {} end
  if(room.items == nil) then room.items = {} end
  if(room.name == nil) then room.name = name end
end

return rooms
