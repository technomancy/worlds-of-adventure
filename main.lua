-- worlds of adventure

local rooms = require("rooms")
local utils = require("utils")

local directions = {"up", "down", "north", "south", "east", "west",
  "northwest", "northeast", "southwest", "southeast"}

local aliases = { s = "south", n = "north", e = "east", w = "west",
  u = "up", d = "down",
  nw = "northwest", ne = "northeast",
  sw = "southwest", se = "southeast", }

local input = ""

local player = {
  inventory = {},
  events = {},
  current_room = "Your bedroom",
}

print("Welcome to the Worlds of Adventure.")

while input ~= "quit" do
  local words = utils.split(input)
  if(aliases[input]) then input = aliases[input] end

  if(type(rooms[player.current_room].actions[input]) == "string") then
    print(rooms[player.current_room].actions[input])
  elseif(type(rooms[player.current_room].actions[input]) == "function") then
    rooms[player.current_room].actions[input](player, rooms)
  elseif(words[1] == "take" and rooms[player.current_room].items[words[2]]) then
    rooms[player.current_room].items[words[2]] = nil
    player.inventory[words[2]] = true
    print("Taken.")
  elseif(words[1] == "take") then
    print("There is no " .. words[2] .. " here.")
  elseif(words[1] == "drop" and player.inventory[words[2]]) then
    rooms[player.current_room].items[words[2]] = true
    player.inventory[words[2]] = nil
    print("Dropped.")
  elseif(words[1] == "drop") then
    print("You don't have any " .. words[2] .. ".")
  elseif(input == "inventory") then
    print("You are carrying:")
    local items = 0
    for i in pairs(player.inventory) do
      items = items + 1
      print("  " .. i)
    end
    if(items == 0) then print("  nothing.") end
  elseif(utils.member(input, directions) and rooms[player.current_room].exits[input]) then
    player.current_room = rooms[player.current_room].exits[input]
  elseif(utils.member(input, directions) and not rooms[player.current_room].exits[input]) then
    print("You can't go that way.")
  elseif(input ~= "") then
    print("I don't know what that is.")
  end
  print(player.current_room)
  print(rooms[player.current_room].description)
  for item in pairs(rooms[player.current_room].items) do
    print("There is a " .. item .. " here.")
  end
  io.write("> ")
  input = io.read()
  print("")
end

print("Good bye. Thank you for playing.")

